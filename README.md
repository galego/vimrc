# README #

Aprendizado e atual configuração do .vimrc

# Fazer funcionar #

1 - Instalar o [Vundle](https://github.com/VundleVim/Vundle.vim "Vundle Repository")
	
	git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

2 - Fazer o Download do arquivo .vimrc e colocá-lo no diretório home/
  
  ~/.vimrc

3 - Abrir o vim e executar o comando:
	
	:PluginInstall
